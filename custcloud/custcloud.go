// <a href="https://godoc.org/github.com/google/go-cloud/blob/s3blob"><img src="https://godoc.org/github.com/google/go-cloud/blob/s3blob?status.svg" alt="GoDoc"></a>

package custcloud

import (
	"context"
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"gocloud.dev/blob"
	"gocloud.dev/blob/fileblob"
	"gocloud.dev/blob/gcsblob"
	"gocloud.dev/blob/s3blob"
	"gocloud.dev/gcp"
)

// type encapsulateBucketSetup interface {
// 	SetupBucket(context.Context, string, string) (*blob.Bucket, error)
// }

// Bucket for encalsulating go-cloud bucket struct
type Bucket struct {
	*blob.Bucket
}

// NewBucket factory function to initialize a go-cloud bucket
func NewBucket(cloud, bucket string) (*Bucket, error) {

	b, err := setupBucket(context.Background(), cloud, bucket)
	if err != nil {
		log.Fatalf("Failed to setup bucket: %s", err)
		return nil, err
	}
	return &Bucket{b}, nil
}

// setupBucket creates a connection to a particular cloud provider's blob storage.
func setupBucket(ctx context.Context, cloud, bucket string) (*blob.Bucket, error) {
	switch cloud {
	case "aws":
		return setupAWS(ctx, bucket)
	case "gcp":
		return setupGCP(ctx, bucket)
	case "local":
		return setupLocalFile(bucket)
	default:
		return nil, fmt.Errorf("invalid cloud provider: %s", cloud)
	}
}

// setupGCP creates a connection to Google Cloud Storage (GCS).
func setupGCP(ctx context.Context, bucket string) (*blob.Bucket, error) {
	// DefaultCredentials assumes a user has logged in with gcloud.
	// See here for more information:
	// https://cloud.google.com/docs/authentication/getting-started
	creds, err := gcp.DefaultCredentials(ctx)
	if err != nil {
		return nil, err
	}
	c, err := gcp.NewHTTPClient(gcp.DefaultTransport(), gcp.CredentialsTokenSource(creds))
	if err != nil {
		return nil, err
	}
	return gcsblob.OpenBucket(ctx, c, bucket, nil)
}

// setupAWS creates a connection to Simple Cloud Storage Service (S3).
func setupAWS(ctx context.Context, bucket string) (*blob.Bucket, error) {
	c := &aws.Config{
		// Either hard-code the region or use AWS_REGION.
		// Region: aws.String("us-east-2"),
		// credentials.NewEnvCredentials assumes two environment variables are
		// present:
		// 1. AWS_ACCESS_KEY_ID, and
		// 2. AWS_SECRET_ACCESS_KEY.
		Credentials: credentials.NewEnvCredentials(),
	}
	s := session.Must(session.NewSession(c))
	return s3blob.OpenBucket(ctx, s, bucket, nil)
}

func setupLocalFile(bucket string) (*blob.Bucket, error) {
	// c, err := fileblob.NewBucket(bucket)
	return fileblob.OpenBucket(bucket, nil)
	// if err != nil {
	// 	return nil, err
	// }
	// return c, nil
}
