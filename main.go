package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"sync"
	"time"

	"gitlab.com/lindep/cloud_upload/fileprocessor"
)

var (
	// Version will be set by Makefile
	Version = "unknown"
	// Build will be set by Makefile
	Build = "unknown"
	// BuildTime will be set by Makefile
	BuildTime = "unknown"
)

var helpString string

func init() {
	const (
		defaultHelp = "help"
		usage       = "Help message"
	)
	flag.StringVar(&helpString, "help", defaultHelp, usage)
	flag.StringVar(&helpString, "h", defaultHelp, usage+" (shorthand)")
}

func verifyPath(path string) (string, *os.File, error) {
	path = filepath.Clean(path)
	f, err := os.Open(path)
	if err != nil {
		// fmt.Println("failed to open ", err)
		return path, nil, err
	}
	return path, f, nil
}

func transDir(path string, dir *os.File, fileLimit int, fprocess *fileprocessor.FileProcessor) error {

	var dirList []string

	for {
		files, err := dir.Readdirnames(fileLimit)
		if err != nil {
			if err == io.EOF {

				for _, dir := range dirList {
					// make sure path is valid and good before processing.
					path, dir, err := verifyPath(dir)
					if err != nil {
						fmt.Println("Path", path, "not valid, not going to try and process")
						continue
					}
					transDir(path, dir, fileLimit, fprocess)
				}
				return nil
			}
			return err
		}

		foundFiles := false
		for _, file := range files {
			filePathName := path + "/" + file
			fi, err := os.Lstat(filePathName)
			if err != nil {
				log.Fatal(err)
				// Stop processing if any error.
				return err
			}

			if mode := fi.Mode(); mode.IsDir() {
				// Do not recursively process the directories
				// Save for later process
				// after the current directory done (EOF) process
				// directories
				dirList = append(dirList, filePathName)
			} else if mode.IsRegular() {
				// Set this valiable so the select
				// only get set if the directory is not empty
				// else skip this directory when empty
				foundFiles = true
				// at this level we don't care about the concurrent metric.
				fprocess.Process(filePathName)
			}
		}
		if foundFiles {
			// fmt.Println("Start timer")
			// Start timer for concurrency timing
			// this will stop recording only when all files done processed
			// which mean the time it took to concurrently process.
			start := time.Now()
			for {
				select {
				case _ = <-fprocess.DoneChan:
					fprocess.DecrRemaining()
				case res := <-fprocess.ResChan():
					fprocess.DecrRemaining()
					fmt.Printf("%s\n", res)
				case err := <-fprocess.ErrChan():
					if err != nil {
						fmt.Println("File error, will skip this file", err)
						// return err
						// break
					}
				}
				if fprocess.GetRemaining() == 0 {
					break
				}
			}
			tC := time.Since(start)
			// fmt.Println("remining done", tC, runtime.NumGoroutine())
			fprocess.IncConTime(tC)
		}
	}
	return nil
}

func main() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s [options] [...dir]\n", os.Args[0])
		flag.PrintDefaults()
	}
	version := flag.Bool("v", false, "Prints current version")

	var totalConcurrectTime, totalRunWithoutCon time.Duration
	// var totalRunWithoutCon time.Duration = 0

	mu := new(sync.RWMutex)
	metrics := make(map[string]string)
	fprocessMapDuration := make(map[string]time.Duration)

	// For aws setup testing to free account
	// cloud = "aws"
	// bucket = "bucket.name"
	cloud := flag.String("cloud", "local", "Cloud provider (aws | gcp | local)")
	bucketName := flag.String("bucket", "/tmp/bucket", "path to the bucket on local disk, default is for bucket on local disk")
	fileLimit := flag.Int("fileLimit", 100, "number of files to scan in a directory")
	processLimit := flag.Int("processLimit", 300, "number of concurrent files to process per directory")

	flag.Parse()
	if *version {
		fmt.Println("Version: ", Version)
		fmt.Println("Build: ", Build)
		fmt.Println("BuildTime: ", BuildTime)
		os.Exit(0)
	}

	fmt.Printf("Cloud provider = (%s), bucket name = (%s)\n", *cloud, *bucketName)

	args := []string{}
	if flag.NArg() > 0 {
		// get all args left after processing flags.
		args = flag.Args()
	}

	var wg = sync.WaitGroup{}
	for _, arg := range args {

		// make sure path is valid and good before creating a routine.
		path, dir, err := verifyPath(arg)
		if err != nil {
			fmt.Println("Path", path, "not valid, not going to try and process")
			continue
		}

		wg.Add(1)
		go func(path string, dir *os.File) {
			// for use with go_cloud
			fileProcess, err := fileprocessor.NewFileProcessor(cloud, bucketName)
			// fileProcess, err := fileprocessor.NewFileProcessor() // when importing from package fileprocessor
			if err != nil {
				wg.Done()
				return
			}
			// fileProcess := NewFileProcessor()
			fileProcess.SetName(path)
			// fprocessMapDuration[path] = fileProcess

			fileProcess.SetConcurreny(*processLimit)
			fileProcess.Put("p", path)
			fileProcess.Get("p")
			processErr := transDir(path, dir, *fileLimit, fileProcess)
			if processErr != nil {
				fmt.Printf("File %s: %v\n", path, processErr)
			}

			ms := fileProcess.PrintMetrics()
			// metrics{fileProcess.GetName(): ms}
			mu.Lock()
			metrics[fileProcess.GetName()] = ms
			mu.Unlock()
			// defer func(f *fileprocessor.FileProcessor) {
			// 	fprocessMapDuration[f.GetName()] = f.GetConTime()
			// 	wg.Done()
			// }(fileProcess)
			defer func() {
				// update fprocessMapDuration slice
				mu.Lock()
				fprocessMapDuration[fileProcess.GetName()] = fileProcess.GetConTime()
				mu.Unlock()
				// below will return object of type *metric.Metric
				// fmt.Printf("-- GetMetrixc return type = %T\n", fileProcess.GetMetrics())
				dir.Close()
				wg.Done()
			}()
		}(path, dir)
	}
	wg.Wait()

	for key, value := range metrics {
		fmt.Println(key, value)
	}

	for key, value := range fprocessMapDuration {
		fmt.Println("Dir", key, "total time", value)
		totalRunWithoutCon += value
		if value > totalConcurrectTime {
			totalConcurrectTime = value
		}
	}

	fmt.Println("-----------------")
	fmt.Println("File list limit =", *fileLimit)
	fmt.Println("Concurrent process limit =", *processLimit)
	fmt.Println("-----------------")
	fmt.Println("Total time would have taken processing:", totalRunWithoutCon)
	fmt.Println("Total time taken processing concurrently:", totalConcurrectTime)
	fmt.Println("Number of CPUs avaialble", runtime.NumCPU(), "Num routines", runtime.NumGoroutine())

}
