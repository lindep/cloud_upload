package custerror

import (
	"fmt"
	"time"
)
type CustError struct {
	When time.Time
	What string
}

func NewError(msg string) *CustError {
	return &CustError{
		time.Now(),
		msg,
	}
}
func (e *CustError) Error() string {
	return fmt.Sprintf("at %v, %s",
		e.When, e.What)
}