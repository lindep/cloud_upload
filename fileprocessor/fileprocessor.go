package fileprocessor

import (
	"fmt"
	"log"
	"sync"
	"time"
	// mrand "math/rand"
	"context"
	"io/ioutil"
	"os"
	"strings"
	// "path/filepath"

	"gitlab.com/lindep/cloud_upload/custerror"
	"gitlab.com/lindep/cloud_upload/custcloud"
	"gitlab.com/lindep/cloud_upload/metric"
)

// type myError struct {
// 	When time.Time
// 	What string
// }

// func (e *myError) Error() string {
// 	return fmt.Sprintf("at %v, %s",
// 		e.When, e.What)
// }

// FileProcessor struct for keep concurrency variables
type FileProcessor struct {
	name      string
	props     map[string]string
	mu        *sync.RWMutex // embedded Lock
	errChan   chan error
	resChan   chan string
	DoneChan  chan bool
	remaining int
	limit     chan struct{}
	count     uint64
	time      time.Duration
	conTime   time.Duration
	metrics   *metric.Metric
	bucket    *custcloud.Bucket
	ctx       context.Context
}

// NewFileProcessor initialize FileProcessor struct
func NewFileProcessor(cloud, bucketName *string) (*FileProcessor, error) {

	ctx := context.Background()

	b, err := custcloud.NewBucket(*cloud, *bucketName)
	if err != nil {
		log.Fatalf("Failed to setup bucket: %s", err)
		return nil, err
	}

	// m := metric{min:0,max:0,avg:0}

	return &FileProcessor{
		name:      "cloud upload processor",
		mu:        new(sync.RWMutex), // not needed as using Lock from embedded
		errChan:   make(chan error),
		resChan:   make(chan string),
		DoneChan:  make(chan bool),
		remaining: 0,
		count:     0,
		time:      0,
		conTime:   0,
		props:     make(map[string]string),
		metrics:   metric.NewMetric(),
		bucket:    b,
		ctx:       ctx,
	}, nil
}

// Put method on FileProcessor struct
func (f *FileProcessor) Put(k, v string) {
	f.mu.Lock()
	f.props[k] = v
	f.mu.Unlock()
}

// Get method on FileProcessor struct
func (f *FileProcessor) Get(k string) string {
	f.mu.RLock()
	v := f.props[k]
	f.mu.RUnlock()
	return v
}

// ResChan return
func (f *FileProcessor) ResChan() <-chan string {
	return f.resChan
}

// ErrChan pass processing error back via channel
func (f *FileProcessor) ErrChan() <-chan error {
	return f.errChan
}

// SetConcurreny set the number of concurrent file copy processes
func (f *FileProcessor) SetConcurreny(l int) error {
	f.mu.Lock()
	defer f.mu.Unlock()
	f.limit = make(chan struct{}, l)
	for i := 0; i < l; i++ {
		f.limit <- struct{}{}
	}
	fmt.Println("set file process concurrent limit to ", l)
	return nil
}

// func (f *FileProcessor) GetConcurreny() int {
//     return f.limit
// }

// incCount increment the count value to keep track of files processed.
func (f *FileProcessor) incCount() {
	f.mu.Lock()
	f.count += 1
	f.mu.Unlock()
}

// GetCount method on FileProcessor struct
func (f *FileProcessor) GetCount() uint64 {
	f.mu.RLock()
	c := f.count
	f.mu.RUnlock()
	return c
}

func (f *FileProcessor) incTime(t time.Duration) {
	f.mu.Lock()
	f.time += t
	f.mu.Unlock()
}

// GetTime method on FileProcessor struct
func (f *FileProcessor) GetTime() time.Duration {
	f.mu.RLock()
	c := f.time
	f.mu.RUnlock()
	return c
}

// IncConTime concurrent time processing metric
// this value is only updated when all concurrent
// go routines are done.
// Doing this to determine how long the concurrent process run
func (f *FileProcessor) IncConTime(t time.Duration) {
	f.mu.Lock()
	f.conTime += t
	f.mu.Unlock()
}

// GetConTime return the concurrent running time
func (f *FileProcessor) GetConTime() time.Duration {
	f.mu.RLock()
	c := f.conTime
	f.mu.RUnlock()
	return c
}

// GetMetrics return a metric pointer
func (f *FileProcessor) GetMetrics() *metric.Metric {
	// f.mu.RLock()
	c := f.metrics.Get()
	// f.mu.RUnlock()
	return c
}

// PrintMetrics return a formatted string of all metrics
func (f *FileProcessor) PrintMetrics() string {
	return f.metrics.Print()
}

// GetName is the name assigned to this process
func (f *FileProcessor) GetName() string {
	// fmt.Println("Printin file name from FileProcessor", f.name)
	return f.name
}

// SetName assign a name to this process for use
// in identifying a specific file process
func (f *FileProcessor) SetName(value string) error {
	f.mu.Lock()
	f.name = value
	f.mu.Unlock()
	//fmt.Println("Printing file name from FileProcessor",name)
	return nil
}

// DecrRemaining keeping track of concurrent processes
// When remaining == 0 the queue is empty and all processing done.
func (f *FileProcessor) DecrRemaining() {
	f.mu.Lock()
	f.remaining--
	f.mu.Unlock()
}

// IncRemaining keeping track of concurrent processes
// process start with a queue.
// every item added to the queue increment the remaining counter.
func (f *FileProcessor) IncRemaining() int {
	f.mu.Lock()
	f.remaining++
	v := f.remaining
	f.mu.Unlock()
	return v
}

// GetRemaining return remaining items in queue
func (f *FileProcessor) GetRemaining() int {
	f.mu.RLock()
	v := f.remaining
	f.mu.RUnlock()
	return v
}

// Process all work is done here
func (f *FileProcessor) Process(path string) int {
	incNum := f.IncRemaining()
	f.metrics.AddConcurrentMax(incNum)

	go func() {
		<-f.limit
		pathRelative := strings.TrimPrefix(path, string(os.PathSeparator))

		start := time.Now()

		defer func() {
			elapsed := time.Since(start)
			f.metrics.Add(elapsed) // handle all metrics for this process.
			f.resChan <- fmt.Sprintf("Done:%s\ttime to process\t%s", path, elapsed)

			// notify channel that this spot is free.
			f.limit <- struct{}{}
		}()

		// Prepare the file for upload.
		data, err := ioutil.ReadFile(path)
		if err != nil {
			log.Print("Failed to read file: %s", err)
			f.errChan <- custerror.NewError(path + " Failed to read file")
			return
			// f.errChan <- &myError{
			// 	time.Now(),
			// 	path + " Failed to read file",
			// }
		}
		w, err := f.bucket.NewWriter(f.ctx, pathRelative, nil)
		if err != nil {
			log.Print("Failed to obtain writer: %s", err)
			f.errChan <- custerror.NewError(path + " Failed to obtain writer")
			return
			// f.errChan <- &myError{
			// 	time.Now(),
			// 	path + " Failed to obtain writer",
			// }
		}

		_, err = w.Write(data)
		if err != nil {
			log.Print("Failed to write to bucket: %s", err)
			f.errChan <- custerror.NewError(path + " Failed to write to bucket")
			return
			// f.errChan <- &myError{
			// 	time.Now(),
			// 	path + " Failed to write to bucket",
			// }
		}

		if err = w.Close(); err != nil {
			log.Print("Failed to close: %s", err)
			f.errChan <- custerror.NewError(path + " Failed to close bucket")
			return
			// f.errChan <- &myError{
			// 	time.Now(),
			// 	path + " Failed to close bucket",
			// }
		}
		
		// DoneChan not used anymore to minimize the select having to 
		// potentially read from 2 channels at same time
		// f.DoneChan <- true
	}()

	return incNum
}
