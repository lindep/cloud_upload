
PROJECTNAME=$(shell basename "$(PWD)")

# GO111MODULES=on

GOCMD=go
GOBIN=/go/bin
GOPATH=/go
GOFILES=$(wildcard *.go)
GOBUILD=$(GOCMD) build
GOTEST=$(GOCMD) test

.PHONY: all clean

# not needed for golang >= v1.11
unexport GOPATH
export CGO_ENABLED=0
export GOOS=linux
export GOARCH=amd64

BINARY_NAME=cloudupload
BINARY_UNIX=$(BINARY_NAME)_unix_$(GOARCH)
SRC_FILES=main.go

VERSION=1.2.10
BUILD=$(shell git rev-parse HEAD)
DATE=$(shell date -u '+%Y-%m-%d %H:%M:%S UTC')
LDFLAGS=-ldflags='-X "main.Version=$(VERSION)" -X "main.Build=$(BUILD)" -X "main.BuildTime=$(DATE)"'

# Redirect error output to a file, so we can show it in development mode.
STDERR=/tmp/.$(PROJECTNAME)-stderr.txt

# PID file will store the server process id when it's running on development mode
PID=/tmp/.$(PROJECTNAME)-api-server.pid

## install: Install missing dependencies. Runs `go get` internally. e.g; make install get=github.com/foo/bar
install: go-get

## build: build app and install in $GOBIN
build:
	@echo "Building..."
	$(GOBUILD) -o ${GOBIN}/$(BINARY_NAME) -v $(LDFLAGS)

## build-linux: build app and install in $GOBIN
build-linux:
	@echo "Building..."
	$(GOBUILD) -o ${GOBIN}/$(BINARY_UNIX) -v $(LDFLAGS)

# go-get:
# 	@echo " > Checking if there is any missing dependencies..."
# 	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go get

## test: not implemented
test:
	@echo "Testing..."
	$(GOTEST) -v ./...

## clean: delete binaries in $GOBIN
clean:
	-rm -f ${GOBIN}/$(BINARY_NAME)
	-rm -f ${GOBIN}/$(BINARY_UNIX)

.PHONY: help
all: help
help: Makefile
	@echo
	@echo " Choose a command run in "$(PROJECTNAME)":"
	@echo
	@sed -n 's/^##//p' $< | sed 's/:/ - /'
	@echo

