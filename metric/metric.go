package metric

import (
	"fmt"
	"sync"
	"time"
)

// Metric struct for app metrics
type Metric struct {
	sync.RWMutex
	Count uint64
	MaxConcurrent int 
	Total, Min, Avg, Max time.Duration
}

// NewMetric factory ti initialize a metric
func NewMetric() *Metric {
	return &Metric{Count: 0, MaxConcurrent: 0, Total: 0, Min: 0, Max: 0, Avg: 0}
}

// Add given a time will add / update all metrices
func (m *Metric) Add(d time.Duration) {
	m.Lock()
	defer func() {
		m.Unlock()
	}()
	m.Count++    // number of files
	m.Total += d // total processing time
	if m.Min == 0 || d < m.Min {
		m.Min = d
	}
	if d > m.Max {
		m.Max = d
	}
}

// Get return the current metric struct
func (m *Metric) Get() *Metric {
	if m.Avg == 0 {
		m.Lock()
		m.Avg = m.getAvg()
		m.Unlock()
	}
	return m
}

func (m *Metric) getAvg() time.Duration {
	// m.RLock()
	// defer func() {
	// 	m.RUnlock()
	// }()
	if m.Total == 0 || m.Count == 0 {
		return 0
	} else {
		return m.Total / time.Duration(m.Count)
	}
}

func (m *Metric) AddConcurrentMax(c int) { 
	m.Lock()
	if m.MaxConcurrent < c {
		m.MaxConcurrent = c
	}
	m.Unlock()
}

// Print metric return a formatted string of all metrics
func (m *Metric) Print() string {
	m.Lock()
	defer func() {
		m.Unlock()
	}()
	// the avg is not calulated on every opertaion done.
	// only calculate the avg once when request to print
	// then only if not been done before.
	if m.Avg == 0 {
		m.Avg = m.getAvg()
	}

	s := fmt.Sprintf("\n------------------\n")
	s = fmt.Sprintf("%sNumber of files = %d\n", s, m.Count)
	s = fmt.Sprintf("%sTotal Time for this set without concurrency = %s\n", s, m.Total)
	s = fmt.Sprintf("%sTime Max = %s\n", s, m.Max)
	s = fmt.Sprintf("%sTime Min = %s\n", s, m.Min)
	s = fmt.Sprintf("%sTime Avg = %s\n", s, m.Avg)
	s = fmt.Sprintf("%sMax concurrency routines per directory = %d\n", s, m.MaxConcurrent)
	s = fmt.Sprintf("%s------------------\n", s)

	return s
}
